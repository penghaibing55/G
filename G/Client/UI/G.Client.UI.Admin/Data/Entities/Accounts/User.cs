﻿using G.Client.UI.Admin.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace G.Client.UI.Admin.Data.Entities.Accounts
{
    public class User : EntityBase<int>
    {
        [Required, StringLength(20, MinimumLength = 5)]
        public string UserName { get; set; }

        [Required, MaxLength(32)]
        public string Password { get; set; }

        [Required, StringLength(20, MinimumLength = 2)]
        public string Name { get; set; }

        [Required, StringLength(20, MinimumLength = 5)]
        public string Role { get; set; }
    }
}