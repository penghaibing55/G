﻿using G.Client.UI.Admin.Data.Entities.Accounts;
using G.Client.UI.Admin.Data.Entities.Base;
using G.Client.UI.Admin.Views.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace G.Client.UI.Admin.Data.Entities.DeployManage
{
    public class DeployServer : EntityBase<int>
    {
        [Required, StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        [Required, StringLength(100, MinimumLength = 2)]
        public string ServerIdentity { get; set; }

        [Required, StringLength(15, MinimumLength = 7)]
        public string IPAddress { get; set; }

        public DeployServerStatus Status { get; set; }
    }
}