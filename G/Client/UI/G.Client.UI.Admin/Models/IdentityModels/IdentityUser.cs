﻿using G.Client.UI.Admin.Data.Entities.Accounts;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G.Client.UI.Admin.Models.IdentityModels
{
    public class IdentityUser : IUser<string>
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }

        public string UserName { get; set; }
    }
}