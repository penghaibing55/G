﻿using G.Client.UI.Admin.Business;
using G.Client.UI.Admin.Models.DeployManageModels.DeployServerModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace G.Client.UI.Admin.Controllers
{
    [Authorize]
    public class DeployServerManageController : Controller
    {
        // GET: DeployManage
        public async Task<ActionResult> Index()
        {
            var deployServerList = await new DeployServerBusiness().GetList();

            return View(deployServerList);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateDeployServerViewModel model)
        {
            ViewBag.IsPostBack = true;

            if (!ModelState.IsValid)
            {
                return View();
            }

            var id = await new DeployServerBusiness().Create(model);

            return View();
        }

        public async Task<ActionResult> Edit(int id)
        {
            var model = await new DeployServerBusiness().FindByID(id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditDeployServerViewModel model)
        {
            ViewBag.IsPostBack = true;

            if (!ModelState.IsValid)
            {
                return View();
            }

            await new DeployServerBusiness().Update(model);

            return View(model);
        }
    }
}