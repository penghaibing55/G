﻿using G.Client.UI.Admin.Views.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G.Client.UI.Admin.Models.DeployManageModels.DeployServerModels
{
    public class DeployServerViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string ServerIdentity { get; set; }

        public string IPAddress { get; set; }

        public DeployServerStatus Status { get; set; }

        public int UpdateUserID { get; set; }

        public string UpdateUserName { get; set; }

        public DateTime UpdateTime { get; set; }
    }
}