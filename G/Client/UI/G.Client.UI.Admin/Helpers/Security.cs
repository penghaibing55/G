﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace G.Client.UI.Admin.Helpers
{
    public class Security
    {
        public static string EntryptPassword(string password)
        {
            return MD5(MD5(password));
        }

        public static string MD5(string data)
        {
            using (MD5 md5 = new MD5CryptoServiceProvider())
            {
                byte[] fromData = Encoding.UTF8.GetBytes(data);
                byte[] targetData = md5.ComputeHash(fromData);
                StringBuilder sb = new StringBuilder(targetData.Length);

                for (int i = 0; i < targetData.Length; i++)
                {
                    sb.Append(targetData[i].ToString("x"));
                }

                return sb.ToString();
            }
        }
    }
}