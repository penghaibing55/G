﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G.Client.UI.Admin.Models.DeployManageModels.DeployServerModels
{
    public class EditDeployServerViewModel: CreateDeployServerViewModel
    {
        public int ID { get; set; }
    }
}